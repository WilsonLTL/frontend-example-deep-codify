export const UPDATE_LOGINDIALOG_STATUS = "UPDATE_LOGINDIALOG_STATUS"

export const updateLoginDialogStatus = () => ({
  type: UPDATE_LOGINDIALOG_STATUS
});