export const INIT_DATA = "INIT_DATA";

export const initDataHandler = () => ({
   type: INIT_DATA
});