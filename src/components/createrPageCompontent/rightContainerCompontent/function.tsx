import { UPDATE_CREATERPAGE_TITLE, UPDATE_CREATERPAGE_DESCRIPTION } from "../../../store/actions/createrPageActions"

export const onChangeTitleListener = (event, dispatch) => {
  dispatch({
    type: UPDATE_CREATERPAGE_TITLE,
    payload: {
      title: event.target.value
    }
  })
}

export const onChangeDescriptionListener = (event, dispatch) => {
  dispatch({
    type: UPDATE_CREATERPAGE_DESCRIPTION,
    payload: {
      description: event.target.value
    }
  })
}