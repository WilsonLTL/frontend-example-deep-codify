import { UPDATE_CREATERPAGE_CONTENT } from "../../../store/actions/createrPageActions"
import { UPDATE_SNACK_BAR } from "../../../store/actions/globalActions/snackBarActions"

export const onClickBackListener = (router) => {
  // Back to home page
  router.push("/")
}

export const onChangeContentListener = (value, dispatch) => {
  dispatch({ 
    type: UPDATE_CREATERPAGE_CONTENT,
    payload: {
      content: value
    }
  })
}

export const onClickSubmitListener = (dispatch, createrPage, user, CreateArticle) => {
  if (createrPage.title === '' || createrPage.description === '' || createrPage.content ==='') {
    // Init Snackbar Message
    dispatch({
      type: UPDATE_SNACK_BAR,
      payload: {
        type: "error",
        message:"請輸入所有資料"
      } 
    })
  } else {
    CreateArticle(
      { 
        variables: {
          articleInput: {
            user_id: user.token,
            title: createrPage.title,
            description: createrPage.description,
            content: createrPage.content
          }
        } 
      }
    );
  }
}

export const onOpenErrorSnackBar = (dispatch, error) => {
  dispatch({
    type: UPDATE_SNACK_BAR,
    payload: {
      type: "error",
      message:"Error:" + error
    } 
  })
}

export const onOpenLoadingSnackBar = (dispatch) => {
  dispatch({
    type: UPDATE_SNACK_BAR,
    payload: {
      type: "info",
      message:"提交文章中"
    } 
  })
}

export const onOpenSuccessSnackBar = (dispatch, router) => {
  dispatch({
    type: UPDATE_SNACK_BAR,
    payload: {
      type: "success",
      message:"已成功發布文章"
    } 
  })
  router.push("/")
}