// React
import React from "react";
// Mui
import { Skeleton } from '@mui/material';
// Compontent
import Header from "../../globalComponent/header"
import ArticleListCompontent from "./articleListCompontent"

const CenterContainerCompontent = ({ t, dispatch, router, user, homePage }) => {

  return (
    <div className="flex flex-col lg:w-6/12 md:w-8/12 w-full">
      <Header dispatch={dispatch} t={t} user={user}/>
      <div　className="px-4 py-4">
        <Skeleton variant="rectangular" height={136} />
      </div>
      <ArticleListCompontent t={t} dispatch={dispatch} router={router} homePage={homePage}/>
    </div>
  );
};

export default CenterContainerCompontent