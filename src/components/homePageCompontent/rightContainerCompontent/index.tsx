// React
import React from "react";
// Compontent
import AuthorsCompontent from "./authorsCompontent"
import SearchBarCompontent from "./searchBarCompontent";
import SeoLinkCompontent from "./seoLinkCompontent";
import TopicsCompontent from "./topicsCompontent";

const RightContainerCompontent = ({ t }) => {
  return (
    <div className="hidden lg:flex flex-col border-l items-center w-3/12 px-6 pb-10">
      <div className="pt-10 space-y-6">
        <SearchBarCompontent t={t}/>
        <TopicsCompontent t={t}/>
        <AuthorsCompontent t={t}/>
        <SeoLinkCompontent t={t}/>
      </div>
    </div>
  );
};

export default RightContainerCompontent;
